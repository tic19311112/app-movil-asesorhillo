import React, {Component} from 'react'
import {Image, View, StyleSheet, StatusBar, TouchableOpacity, AppRegistry} from 'react-native'
import {Actions} from 'react-native-router-flux'

class Splash extends Component{
    render() {
        return (
            <>
                <StatusBar barStyle="dark-content" hidden={false} translucent={ true} backgroundColor='#85A6C7'/>
                <View style={styles.container}>
                    <View style={styles.imgContainer}>
                        <TouchableOpacity >
                            <Image source={require('./LogoRedondo.png')} style={styles.logo} />
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        )
    }
}

   // const goToHome = ()=>{
     //   Actions.Home()
   // }
export default Splash
//AppRegistry.registerComponent('Splash', ()=>Splash)

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
    },
    imgContainer: {
        flex:1,
        height: '100%',
        justifyContent:'center'
    },
    logo: {
        width: 200,
        height:200
    }
})
