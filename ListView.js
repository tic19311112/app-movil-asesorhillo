import React,{Component} from 'react'
import {Text, TouchableOpacity, View, StyleSheet, StatusBar, ScrollView, Image, Alert} from 'react-native'

class ListView extends Component{

    state= {
        data:[]
    }

    componentDidMount= () => {
        fetch('https://ramon.uthds4.info/api/cursos', {method: 'GET'})
        .then((response)=>response.json())
        .then((responseJson)=>{
            console.log(responseJson);
            this.setState({
                data: responseJson
            })
        })
        .catch((error)=>{
            console.error(error);
        });
    }

    alertTitle=(item)=>{
        Alert.alert("Descripción",item.descrip+"\n" + "\nTiene un precio de: "+item.precio+" pesos");
    }

    render(){
        return(
            <>
                <ScrollView>
                <StatusBar StatusBar barStyle="dark-content" hidden={false} translucent={ true} backgroundColor='#85A6C7' translucent = {true}/>
                    <View style={styles.container}>
                    <Text style={styles.title}>Cursos Disponibles</Text>
                        {
                            this.state.data.map((item, index)=>(
                                <View style={styles.container2}>
                                <TouchableOpacity key={item.id} onPress={() => this.alertTitle(item)}>
                                    <Image source={require('./img/foto1.png')} style={styles.logo} />
                                    <Text style={{fontSize:20, paddingTop:12}}>{item.nombre}</Text>
                                </TouchableOpacity>
                                </View>
                            ))
                        }
                    </View>
                </ScrollView>
            </>
        )
    }
}

export default ListView

const styles= StyleSheet.create({
    container:{
        marginTop: 50,
        alignItems: 'center',
        borderRadius: 20,
        margin:50
    },
    logo:{
        marginTop: 30,
        alignItems: 'center',
        width:455,
        borderRadius:10
    },
    title: {
        fontSize: 50,
        alignItems: 'center',
    
    }

})
